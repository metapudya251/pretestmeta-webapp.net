﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {

        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            repeaterUser.DataSource = db.TBUsers.Select(x => new
            {
                x.ID,
                x.UID,
                x.Name,
                NameCompany = x.TBCompany.Name,
                NamePosition = x.TBPosition.Name,
                x.Address,
                x.Email,
                x.Telephone

            }).ToArray();
            repeaterUser.DataBind();
        }
    }

    protected void repeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Form.aspx?id=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                ControllerUser user = new ControllerUser(db);
                user.Delete(int.Parse(e.CommandArgument.ToString()));
                db.SubmitChanges();

                LoadData();
            }
        }
    }

}