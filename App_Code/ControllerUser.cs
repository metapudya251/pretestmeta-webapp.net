﻿                                                                                                                                                                                               using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for ControllerUser
/// </summary>
public class ControllerUser : ClassBase
{
    public ControllerUser(DataClassesPretestDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.TBUsers.Select(x => new
        {
            x.ID,
            x.IDCompany,
            x.IDPosition,
            NameCompany = x.TBCompany.Name,
            NamePosition = x.TBCompany.Name,
            x.Name,
            x.Address,
            x.Email,
            x.Telephone
        }).ToArray();

    }

    public TBUser Create(int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        var data = Encoding.ASCII.GetBytes(password);
        var md5 = new MD5CryptoServiceProvider();
        var md5data = md5.ComputeHash(data);

        TBUser user = new TBUser
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDPosition = IDPosition,
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            Username = username,
            Password = Convert.ToBase64String(md5data),
            Role = "Admin",
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.TBUsers.InsertOnSubmit(user);

        return user;
    }

    public TBUser Cari(int ID)
    {
        return db.TBUsers.FirstOrDefault(x => x.ID == ID);
    }

    public TBUser Update(int ID, int IDCompany, int IDPosition, string name, string address, string email, string telephone, string username, string password)
    {
        var user = Cari(ID);
        var data = Encoding.ASCII.GetBytes(password);
        var md5 = new MD5CryptoServiceProvider();
        var md5data = md5.ComputeHash(data);

        if (user != null)
        {
            user.IDCompany = IDCompany;
            user.IDPosition = IDPosition;
            user.Name = name;
            user.Address = address;
            user.Email = email;
            user.Telephone = telephone;
            user.Username = username;
            user.Password = Convert.ToBase64String(md5data);
            user.Role = "Admin";
            user.Flag = 1;
            user.CreatedBy = 1;

            return user;
        }
        else return user;
    }

    public TBUser Delete(int ID)
    {
        var user = Cari(ID);
        if (user != null)
        {
            db.TBUsers.DeleteOnSubmit(user);
            db.SubmitChanges();
            return user;
        }
        else
        {
            return null;
        }
    }

}