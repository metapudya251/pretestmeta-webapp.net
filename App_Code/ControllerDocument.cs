﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ControllerDocument
/// </summary>
public class ControllerDocument : ClassBase
{
    public ControllerDocument(DataClassesPretestDataContext _db) : base(_db)
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void ViewData()
    {
        db.TBDocuments.Select(x => new
        {
            x.ID,
            x.UID,
            x.Name,
            NameCompany = x.TBCompany.Name,
            NameCategory = x.TBDocument_Category.Name,
            x.Description

        }).ToArray();
    }

    public TBDocument Create(int IDCompany, int IDCategory, string name, string description)
    {
        TBDocument document = new TBDocument
        {
            UID = Guid.NewGuid(),
            IDCompany = IDCompany,
            IDCategory = IDCategory,
            Name = name,
            Description = description,
            Flag = 1,
            CreatedBy = 1,
            CreatedAt = DateTime.Now,

        };

        db.TBDocuments.InsertOnSubmit(document);
        return document;
    }
    public TBDocument Cari(int ID)
    {
        return db.TBDocuments.FirstOrDefault(x => x.ID == ID);
    }

    public TBDocument Update(int ID, int IDCompany, int IDCategory, string name, string description)
    {
        var document = Cari(ID);

        if (document != null)
        {
            document.IDCompany = IDCompany;
            document.IDCategory = IDCategory;
            document.Name = name;
            document.Description = description;
            document.Flag = 1;
            document.CreatedBy = 1;

            return document;
        }
        else
        {
            return null;
        }
    }
    public TBDocument Delete(int ID)
    {
        var document = Cari(ID);
        if (document != null)
        {
            db.TBDocuments.DeleteOnSubmit(document);
            db.SubmitChanges();
            return document;
        }
        else
        {
            return null;
        }
    }

}