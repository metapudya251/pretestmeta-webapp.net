﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerDocument controllerDocument = new ControllerDocument(db);

                ControllerCompany controllerCompany = new ControllerCompany(db);
                listCompany.Items.AddRange(controllerCompany.DropDownList());

                ControllerCategory controllerCategory = new ControllerCategory(db);
                listCategory.Items.AddRange(controllerCategory.DropDownList());


                var doc = controllerDocument.Cari(Convert.ToInt32(Request.QueryString["id"]));

                if (doc != null)
                {
                    listCompany.SelectedValue = doc.IDCompany.ToString();
                    listCategory.SelectedValue = doc.IDCategory.ToString();
                    inputName.Text = doc.Name;
                    inputDescription.Text = doc.Description;

                    btnOk.Text = "Update";

                }
                else
                {
                    btnOk.Text = "Add New";
                }
            }
        }
    }

    protected void BtnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerDocument doc = new ControllerDocument(db);

                if (btnOk.Text == "Add New")
                {
                    doc.Create(int.Parse(listCompany.SelectedValue), int.Parse(listCategory.SelectedValue), inputName.Text, inputDescription.Text);
                }
                else if (btnOk.Text == "Update")
                {
                    doc.Update(int.Parse(Request.QueryString["id"]), int.Parse(listCompany.SelectedValue), int.Parse(listCategory.SelectedValue), inputName.Text, inputDescription.Text);
                }
                db.SubmitChanges();

                Response.Redirect("/Document/Default.aspx");

            }
        }
    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Document/Default.aspx");
    }
}