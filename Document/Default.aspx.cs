﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            repeaterDocument.DataSource = db.TBDocuments.Select(x => new
            {
                x.ID,
                x.UID,
                x.Name,
                NameCompany = x.TBCompany.Name,
                NameCategory = x.TBDocument_Category.Name,
                x.Description

            }).ToArray();
            repeaterDocument.DataBind();
        }
    }

    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Form.aspx?id=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                ControllerDocument doc = new ControllerDocument(db);
                doc.Delete(int.Parse(e.CommandArgument.ToString()));
                db.SubmitChanges();

                LoadData();
            }
        }
    }

}