USE [master]
GO
/****** Object:  Database [PretestMeta]    Script Date: 08/04/2023 08:59:00 ******/
CREATE DATABASE [PretestMeta] ON  PRIMARY 
( NAME = N'PretestMeta', FILENAME = N'D:\MSSQLSERVER\PretestMeta.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PretestMeta_log', FILENAME = N'D:\MSSQLSERVER\PretestMeta_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PretestMeta] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PretestMeta].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PretestMeta] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [PretestMeta] SET ANSI_NULLS OFF
GO
ALTER DATABASE [PretestMeta] SET ANSI_PADDING OFF
GO
ALTER DATABASE [PretestMeta] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [PretestMeta] SET ARITHABORT OFF
GO
ALTER DATABASE [PretestMeta] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [PretestMeta] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [PretestMeta] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [PretestMeta] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [PretestMeta] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [PretestMeta] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [PretestMeta] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [PretestMeta] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [PretestMeta] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [PretestMeta] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [PretestMeta] SET  DISABLE_BROKER
GO
ALTER DATABASE [PretestMeta] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [PretestMeta] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [PretestMeta] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [PretestMeta] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [PretestMeta] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [PretestMeta] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [PretestMeta] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [PretestMeta] SET  READ_WRITE
GO
ALTER DATABASE [PretestMeta] SET RECOVERY FULL
GO
ALTER DATABASE [PretestMeta] SET  MULTI_USER
GO
ALTER DATABASE [PretestMeta] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [PretestMeta] SET DB_CHAINING OFF
GO
USE [PretestMeta]
GO
/****** Object:  Table [dbo].[TBCompany]    Script Date: 08/04/2023 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBCompany](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBCompany] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBCompany] ON
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'77e34d73-9cd2-4cb2-8990-78b9e6a2faca', N'UAJY', N'Babrsari', N'test@gmail.com', N'0812346393843', 1, 1, NULL)
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'b7ae9f51-9a41-448f-b89b-28fed259a8dd', N'string123', N'string', N'string', N'string', 0, 1, CAST(0x0000B05300BDE79E AS DateTime))
INSERT [dbo].[TBCompany] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (3, N'e8aacb9e-bd76-48e6-81ae-6be88e759855', N'Tes', N'string', N'string', N'string', 0, 1, CAST(0x0000B05300BDF607 AS DateTime))
SET IDENTITY_INSERT [dbo].[TBCompany] OFF
/****** Object:  Table [dbo].[TBPosition]    Script Date: 08/04/2023 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBPosition](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBPosition] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBPosition] ON
INSERT [dbo].[TBPosition] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'3a0263d3-4b12-4aa5-b399-cd14ae1eb59b', N'Testing', 1, CAST(0x0000B05300C0C242 AS DateTime))
INSERT [dbo].[TBPosition] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (4, N'd6bb0077-00cc-4347-961e-10f6491da6a7', N'Head', 1, CAST(0x0000B05301818828 AS DateTime))
SET IDENTITY_INSERT [dbo].[TBPosition] OFF
/****** Object:  Table [dbo].[TBDocument_Category]    Script Date: 08/04/2023 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBDocument_Category](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocument_Category] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBDocument_Category] ON
INSERT [dbo].[TBDocument_Category] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'c6043870-1df9-4f1d-bfb3-fda34509ad63', N'string', 2, CAST(0x0000B05300C4B607 AS DateTime))
INSERT [dbo].[TBDocument_Category] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (3, N'0651f6a4-305e-4fc0-b2b1-1de57b756db1', N'Category 1', 1, CAST(0x0000B05301838D70 AS DateTime))
INSERT [dbo].[TBDocument_Category] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (4, N'e645531f-f02e-4cbb-872f-207b6a55be53', N'Category 2', 1, CAST(0x0000B0530183958A AS DateTime))
SET IDENTITY_INSERT [dbo].[TBDocument_Category] OFF
/****** Object:  Table [dbo].[TBDocument]    Script Date: 08/04/2023 08:59:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBDocument](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBDocument] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBDocument] ON
INSERT [dbo].[TBDocument] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'97040735-7051-484a-a6e0-25e546da3a96', 1, 1, N'string', N'string', 0, 0, CAST(0x0000B05300C94AF9 AS DateTime))
INSERT [dbo].[TBDocument] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'60e8b1bb-3255-4bd6-bfce-2f32591b81d8', 1, 1, N'tes', N'string', 0, 0, CAST(0x0000B05300C97006 AS DateTime))
SET IDENTITY_INSERT [dbo].[TBDocument] OFF
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateCompany]
	@id int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Update statements for procedure here
	UPDATE [dbo].TBCompany SET
           [Name]=@name
           ,[Address]=@address
           ,[Email]=@email
           ,[Telephone]=@telephone
           ,[Flag]=@flag
           ,[CreatedBy]=@iduser
     WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  Table [dbo].[TBUser]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBUser](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_TBUser] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TBUser] ON
INSERT [dbo].[TBUser] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (6, N'5d9a156b-894b-457c-af01-ad9d53fa566e', 1, 2, N'Meta', N'string', N'admin@admin.com', N'string', N'string', N'21232F297A57A5A743894A0E4A801FC3', N'Admin', 0, 1, CAST(0x0000B05300CB2D67 AS DateTime))
INSERT [dbo].[TBUser] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (7, N'a3a21e20-54de-4575-a75d-c4502be6eb81', 1, 2, N'Testing', N'string', N'admin@admin.com', N'string', N'string', N'21232F297A57A5A743894A0E4A801FC3', N'Admin', 0, 1, CAST(0x0000B05300CB5005 AS DateTime))
SET IDENTITY_INSERT [dbo].[TBUser] OFF
/****** Object:  StoredProcedure [dbo].[sp_CreateCompany]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateCompany]
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Insert statements for procedure here
	INSERT INTO [PretestMeta].[dbo].[TBCompany] 
           ([UID]
           ,[Name]
           ,[Address]
           ,[Email]
           ,[Telephone]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@name
           ,@address
           ,@email
           ,@telephone
           ,@flag
           ,@iduser
           ,GETDATE())
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreatePosition]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreatePosition]
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Insert statements for procedure here
	INSERT INTO [PretestMeta].[dbo].[TBPosition]
           ([UID]
           ,[Name]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@name
           ,@iduser
           ,GETDATE())
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument_Category]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument_Category] 
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PretestMeta].[dbo].[TBDocument_Category]
           ([UID]
           ,[Name]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@name
           ,@iduser
           ,GETDATE())
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosition]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdatePosition]
	@id int,
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Insert statements for procedure here
	UPDATE [dbo].TBPosition SET
           [Name]=@name
           ,[CreatedBy]=@iduser
     WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument_Category]    Script Date: 08/04/2023 08:59:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument_Category] 
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here
	UPDATE [dbo].[TBDocument_Category] SET
           [Name]=@name
           ,[CreatedBy]=@iduser
     WHERE ID = @id
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  View [dbo].[vw_User]    Script Date: 08/04/2023 08:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_User]
AS
SELECT     dbo.TBUser.ID, dbo.TBUser.UID, dbo.TBUser.IDCompany, dbo.TBCompany.Name AS Company, dbo.TBUser.IDPosition, dbo.TBPosition.Name AS Position, dbo.TBUser.Name, dbo.TBUser.Address, 
                      dbo.TBUser.Email, dbo.TBUser.Telephone, dbo.TBUser.Username, dbo.TBUser.Password, dbo.TBUser.Role, dbo.TBUser.Flag, dbo.TBUser.CreatedBy
FROM         dbo.TBUser INNER JOIN
                      dbo.TBCompany ON dbo.TBUser.IDCompany = dbo.TBCompany.ID INNER JOIN
                      dbo.TBPosition ON dbo.TBUser.IDPosition = dbo.TBPosition.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[25] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -384
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "TBPosition"
            Begin Extent = 
               Top = 6
               Left = 632
               Bottom = 126
               Right = 792
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBUser"
            Begin Extent = 
               Top = 108
               Left = 293
               Bottom = 228
               Right = 453
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_User'
GO
/****** Object:  View [dbo].[vw_document]    Script Date: 08/04/2023 08:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_document]
AS
SELECT     dbo.TBDocument.*, dbo.TBCompany.Name AS Company, dbo.TBDocument_Category.Name AS Category
FROM         dbo.TBCompany INNER JOIN
                      dbo.TBDocument ON dbo.TBCompany.ID = dbo.TBDocument.IDCompany INNER JOIN
                      dbo.TBDocument_Category ON dbo.TBDocument.IDCategory = dbo.TBDocument_Category.ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "TBCompany"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 126
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocument"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 126
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "TBDocument_Category"
            Begin Extent = 
               Top = 6
               Left = 434
               Bottom = 126
               Right = 594
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_document'
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 08/04/2023 08:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateDocument] 
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idcategory int,
	@name varchar(50),
	@description text,
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here
	UPDATE [dbo].TBDocument SET
		   [IDCompany]=@idcompany
           ,[IDCategory] = @idcategory
           ,[Name]=@name
           ,[Description]=@description
           ,[Flag]=@flag
           ,[CreatedBy]=@iduser
     WHERE ID = @id
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 08/04/2023 08:59:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateDocument] 
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idcategory int,
	@name varchar(50),
	@description text,
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PretestMeta].[dbo].[TBDocument]
           ([UID]
           ,[IDCompany]
           ,[IDCategory]
           ,[Name]
           ,[Description]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@idcompany
           ,@idcategory
           ,@name
           ,@description
           ,@flag
           ,@iduser
           ,GETDATE())
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  UserDefinedFunction [dbo].[func_GetUser]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[func_GetUser] 
(	
	-- Add the parameters for the function here
	@id int
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT * from [PretestMeta].[dbo].[TBUser]
)
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_UpdateUser] 
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(50),
	@role varchar(50),
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Update statements for procedure here
	UPDATE [dbo].[TBUser] SET
           [IDCompany]=@idcompany
           ,[IDPosition]=@idposition
           ,[Name]=@name
           ,[Address]=@address
           ,[Email]=@email
           ,[Telephone]=@telephone
           ,[Username]=@username
           ,[Password]=CONVERT(VARCHAR(32),HashBytes('MD5',@password),2)
           ,[Role]=@role
           ,[Flag]=@flag
           ,[CreatedBy]=@iduser
     WHERE ID = @id
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(50),
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	From TBUser
	Where [Email] = @email and [Password] = CONVERT(VARCHAR(32),HashBytes('MD5',@password),2)
           
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteUser]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	
	-- Delete statements for procedure here
	DELETE FROM [dbo].TBUser WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeletePosition]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeletePosition]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Delete User
	DELETE FROM [dbo].[TBUser]
	WHERE IDPosition = @id
	
	-- Delete statements for procedure here
	DELETE FROM [dbo].TBPosition WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument_Category]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocument_Category]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Delete Document
	DELETE FROM [dbo].[TBDocument]
	WHERE IDCategory = @id
	
	-- Delete statements for procedure here
	DELETE FROM [dbo].TBDocument_Category WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteDocument]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	
	-- Delete statements for procedure here
	DELETE FROM [dbo].TBDocument WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteCompany]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_DeleteCompany]
	@id int,
	@retVal int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
	-- Delete User
	DELETE FROM [dbo].[TBUser]
	WHERE IDCompany = @id
	
	-- Delete Document
	DELETE FROM [dbo].[TBDocument]
	WHERE IDCompany = @id
	
	-- Delete statements for procedure here
	DELETE FROM [dbo].TBCompany WHERE ID = @id
	
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 08/04/2023 08:59:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateUser] 
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(50),
	@role varchar(50) OUTPUT,
	@flag int,
	@iduser int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [PretestMeta].[dbo].[TBUser]
           ([UID]
           ,[IDCompany]
           ,[IDPosition]
           ,[Name]
           ,[Address]
           ,[Email]
           ,[Telephone]
           ,[Username]
           ,[Password]
           ,[Role]
           ,[Flag]
           ,[CreatedBy]
           ,[CreatedAt])
     VALUES
           (NEWID()
           ,@idcompany
           ,@idposition
           ,@name
           ,@address
           ,@email
           ,@telephone
           ,@username
           ,CONVERT(VARCHAR(32),HashBytes('MD5',@password),2)
           ,@role
           ,@flag
           ,@iduser
           ,GETDATE())
	IF(@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  ForeignKey [FK_TBDocument_TBCompany]    Script Date: 08/04/2023 08:59:00 ******/
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBCompany]
GO
/****** Object:  ForeignKey [FK_TBDocument_TBDocument_Category]    Script Date: 08/04/2023 08:59:00 ******/
ALTER TABLE [dbo].[TBDocument]  WITH CHECK ADD  CONSTRAINT [FK_TBDocument_TBDocument_Category] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[TBDocument_Category] ([ID])
GO
ALTER TABLE [dbo].[TBDocument] CHECK CONSTRAINT [FK_TBDocument_TBDocument_Category]
GO
/****** Object:  ForeignKey [FK_TBUser_TBCompany]    Script Date: 08/04/2023 08:59:01 ******/
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBCompany] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[TBCompany] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBCompany]
GO
/****** Object:  ForeignKey [FK_TBUser_TBPosition]    Script Date: 08/04/2023 08:59:01 ******/
ALTER TABLE [dbo].[TBUser]  WITH CHECK ADD  CONSTRAINT [FK_TBUser_TBPosition] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[TBPosition] ([ID])
GO
ALTER TABLE [dbo].[TBUser] CHECK CONSTRAINT [FK_TBUser_TBPosition]
GO
