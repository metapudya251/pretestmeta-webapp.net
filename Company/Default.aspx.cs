﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataClassesPretestDataContext cntxt = new DataClassesPretestDataContext();
            var results = from TBCompany in cntxt.TBCompanies select TBCompany;
            rep1.DataSource = results;
            rep1.DataBind();
        }
    }

    protected void repeaterCompany_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Company/Form.aspx?uid=" + e.CommandArgument);
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.TBCompanies.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                ControllerCompany controllerCompany = new ControllerCompany(db);
                controllerCompany.Delete(e.CommandArgument.ToString());

                //db.TBCompanies.DeleteOnSubmit(company);
                db.SubmitChanges();

                Response.Redirect("/Company/Default.aspx");
            }
        }
    }

    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        DataClassesPretestDataContext dbcompany = new DataClassesPretestDataContext();
        //company.Create(inputName.Value, inputAddress.Value, inputTelephone.Value);

        ControllerCompany company = new ControllerCompany(dbcompany);

        //company.Delete(e);

    }


}