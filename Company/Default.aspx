﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Company_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" Runat="Server">
    <div class="row">
        <div class="col-md-7">
            <h4>Company List</h4>
        </div>

        <div class="col-md-5">
            <div class="d-flex justify-content-sm-end">
                <a href="Form.aspx" class="btn btn-success btn-icon-split" role="button">
                    <span class="text-white-50 icon"><i class="bi bi-check"></i></span>
                    <span class="text-white text">Add New</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
            <table class="table table-striped my-0">
                <thead>
                    <tr class="text-center">
                        <th scope="col">No</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Address</th>
                        <th scope="col">Telephone</th>
                        <th class="text-center">Action</th>
                    </tr>

                </thead>
                <tbody>
                    <asp:Repeater ID="rep1" runat="server" OnItemCommand="repeaterCompany_ItemCommand">
                        <ItemTemplate>
                            <tr class="text-center">
                                <td><%# Container.ItemIndex + 1 %></td>
                                <td><%# Eval("Name") %></td>
                                <td><%# Eval("Address") %></td>
                                <td><%# Eval("Telephone") %></td>
                                <td class="text-center">
                                    <asp:LinkButton runat="server" CommandName="Update" CommandArgument='<%# Eval ("UID") %>' ><i class="bi bi-info-circle text-success h4"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" CommandArgument='<%# Eval ("UID") %>' OnClientClick='<%# "return confirm(\"Apakah anda yakin menghapus data " + Eval("Name") + "\")" %>'><i class="bi bi-trash text-danger h4 ms-2"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" Runat="Server">
</asp:Content>

