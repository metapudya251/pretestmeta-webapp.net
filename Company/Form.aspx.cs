﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                var company = controllerCompany.Cari(Request.QueryString["uid"]);

                if (company != null)
                {
                    inputName.Text = company.Name;
                    inputAddress.Text = company.Address;
                    inputTelephone.Text = company.Telephone;

                    btnOk.Text = "Update";

                }
                else
                {
                    btnOk.Text = "Add New";
                }
            }
        }
    }

    protected void BtnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerCompany company = new ControllerCompany(db);

                if (btnOk.Text == "Add New")
                {
                    company.Create(inputName.Text, inputAddress.Text, inputEmail.Text, inputTelephone.Text);
                }
                else if (btnOk.Text == "Update")
                {
                    company.Update(Request.QueryString["uid"], inputName.Text, inputAddress.Text, inputEmail.Text, inputTelephone.Text);
                }
                db.SubmitChanges();

                Response.Redirect("/Company/Default.aspx");

            }
        }
    }


    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Company/Default.aspx");
    }

}