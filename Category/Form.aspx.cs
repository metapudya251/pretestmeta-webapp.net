﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Category_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerCategory category = new ControllerCategory(db);

                var data = category.Cari(Request.QueryString["uid"]);

                if (data != null)
                {
                    inputName.Text = data.Name;

                    btnOk.Text = "Update";
                }
                else
                {
                    btnOk.Text = "Add New";
                }
            }
        }
    }

    protected void BtnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (DataClassesPretestDataContext db = new DataClassesPretestDataContext())
            {
                ControllerCategory category = new ControllerCategory(db);

                if (btnOk.Text == "Add New")
                {
                    category.Create(inputName.Text);
                }
                else if (btnOk.Text == "Update")
                {
                    category.Update(Request.QueryString["uid"], inputName.Text);
                }
                db.SubmitChanges();

                Response.Redirect("/Category/Default.aspx");

            }
        }
    }


    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Category/Default.aspx");
    }
}